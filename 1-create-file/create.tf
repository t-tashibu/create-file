# AWSのCloudShell上で実行

# 1.SSH で利用するキーペアの作成
aws ec2 create-key-pair  \
  --key-name k8s-hands-on \
  --query 'KeyMaterial' \
  --output text > k8s-hands-on.pem

# 2.catコマンドでpemファイルを表記してコピーを行う
cat k8s-hands-on.pem
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA4146CLUE1UnlU85S4Tdp2Jk3jkpaup+7d06vLY9RQZGXY1qO
　　　　　　　　　~~以下略~~
-----END RSA PRIVATE KEY-----

# 3.Security Groupの作成
aws ec2 create-security-group \
  --group-name hands-on \
  --description "hands-on"

# Security Groupルールの更新
SECGROUP_ID=`aws ec2 describe-security-groups --group-names 'hands-on' --query 'SecurityGroups[*].[GroupId]' --output text`
for PORT in 22 80 443 8080 8443 18080 18443 28080 28443; do
  aws ec2 authorize-security-group-ingress \
  --group-id ${SECGROUP_ID} \
  --protocol tcp \
  --cidr 0.0.0.0/0 \
  --port ${PORT}
done

# インスタンスの起動（Ubuntu 22.04 image）
aws ec2 run-instances \
  --image-id ami-09a81b370b76de6a2 \
  --count 1 \
  --instance-type t2.xlarge \
  --block-device-mappings '[{"DeviceName":"/dev/sda1","Ebs":{"VolumeSize":50}}]' \
  --key-name k8s-hands-on \
  --security-group-ids ${SECGROUP_ID} \
  --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=hands-on}]'

# インスタンスのIPアドレスの確認
aws ec2 describe-instances \
  --filters "Name=tag:Name,Values=hands-on" \
  --query 'Reservations[*].Instances[*].PublicIpAddress' \
  --output text
#VMのIPが出力されるので、それを[YOUR_VM_IP_ADDRESS]を当てはめる

# VMセットアップ
sudo apt-get update
sudo apt-get install -y curl vim git unzip gnupg lsb-release ca-certificates dstat
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# rootでsshアクセスするための設定
#!/bin/bash
sudo hostnamectl set-hostname controlplane

sudo su
cp /home/ubuntu/.ssh/authorized_keys /root/.ssh/authorized_keys

#SwapのOff
swapoff -a

# 名前解決の設定
cd /etc/hosts

# [YOUR_VM_IP_ADDRESS]はVMのIPアドレスを設定
YOUR_VM_IP_ADDRESS    app.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    prometheus.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    grafana.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    jaeger.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    argocd.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    app.argocd.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    dev.kustomize.argocd.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    prd.kustomize.argocd.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    helm.argocd.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    app-preview.argocd.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    kiali.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    app.cilium.vm.hands-on.jp
YOUR_VM_IP_ADDRESS    hubble.cilium.vm.hands-on.jp

# リポジトリのClone
git clone https://github.com/cloudnativedaysjp/cndt2023-handson.git
