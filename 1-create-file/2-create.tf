# cdでディレクトリ実行を行う個所に移動する
cd cndt2023-handson/chapter01_cluster-create/

# インストールツールの実行
./install-tools.sh

# カーネルパラメータの修正と永続化
sudo sysctl fs.inotify.max_user_watches=524288
sudo sysctl fs.inotify.max_user_instances=512

cat << EOF | sudo tee /etc/sysctl.conf >/dev/null
fs.inotify.max_user_watches = 524288
fs.inotify.max_user_instances = 512
EOF

#クラスター構築 この時点でCNIプラグインはされていないの注意
sudo kind create cluster --config=kind-config.yaml

# GateWayAPIのCRDをデプロイ
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.0.0/config/crd/standard/gateway.networking.k8s.io_gatewayclasses.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.0.0/config/crd/standard/gateway.networking.k8s.io_gateways.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.0.0/config/crd/standard/gateway.networking.k8s.io_httproutes.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.0.0/config/crd/standard/gateway.networking.k8s.io_referencegrants.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.0.0/config/crd/experimental/gateway.networking.k8s.io_tlsroutes.yaml

# Cilium、Metallb、Ingress-NGINX-Controllerをhelmでデプロイ
helmfile sync -f helm/helmfile.yaml

kubectl apply -f manifest/metallb.yaml

# kubectlコマンドの補完の有効化
source <(kubectl completion bash)
echo 'source <(kubectl completion bash)' >>~/.bashrc

# クラスター接続情報の確認

